## 1.1.0（2024-09-27）
* 修复快速滑动，上拉和下拉加载有几率不完全显示的问题
## 1.0.9（2024-05-27）
* 增加autoplay和interval属性控制自动播放
## 1.0.8（2024-03-29）
* 修复app-vue初始化报错，并且无法翻页的问题
## 1.0.7（2024-03-29）
* 恢复微信小程序使用插槽的方式
* 保留微信小程序引入子组件的方式
## 1.0.6（2024-03-06）
* 添加vue3兼容
* 修改微信小程序使用方式（为了提高性能以及兼容vue3）如果使用过该组件编写微信小程序的开发者，没有什么问题可以不用更新，避免出错
## 1.0.5（2023-11-13）
* 增加pc端兼容
## 1.0.4（2023-09-02）
* 解决插槽中有scroll-view等滚动组件时滑动报错的问题
## 1.0.3（2023-08-23）
* 解决APP-NVUE端resetLoading不生效的问题
## 1.0.2（2023-07-25）
* 解决竖直翻页时，点击上下2侧翻页不准确得问题
## 1.0.1（2023-07-24）
* 解决APP-NVUE无法点击左右2侧翻页得问题
## 1.0.0（2023-07-22）
* 第一次更新
