export default {
	computed: {
		currentProgress () {
			return this.pages[this.current]?.progress || 0
		},
		currentTitle () {
			return this.pages[this.current]?.title || this.title
		},
		currentTotal () {
			return this.pages[this.current]?.total || 0
		},
		currentPage () {
			return this.pages[this.current]?.current || 0
		}
	},
	methods: {
		//渲染页面
		scrollRender ({chapter, current, start = 0}) {
			if ( chapter ) {//如果传入章节内容
				const index = this.chapters.findIndex(c => c.index == chapter.index)//是否已经包含相同章节
				if (index > -1) this.chapters[index] = chapter//如果包含则更新
				else this.chapters.push(chapter)//否则添加新章节
			}
			current = parseInt(current || 0)//强制转换类型为int
			start = parseInt(start)//强制转换类型为int
			this.pages = [{index: current, type: 'loading'}]//显示loading
			this.current = 0//重置current
			const cgs = this.chapters.filter(c => c.index == current || c.index == current - 1 || c.index == current + 1)//筛选出符合条件的三个章节内容
			let index = 0, arr = []
			const computedId = this.getComputedId()
			this.computeds.push(computedId)
			const computedIndex = this.computeds.indexOf(computedId)
			this.$nextTick(function () {
				setTimeout(() => {
					const handle = () => {
						this.$refs.computed[computedIndex] && this.$refs.computed[computedIndex].start({
							chapter: cgs[index],
							success: pages => {
								arr = arr.concat(pages)
								if ( index < cgs.length - 1 ) {
									index++
									this.$nextTick(function () { handle() })
								} else {
									this.computeds.splice(computedIndex, 1)
									this.pages = this.handlePages(arr)
									const pageIndex = this.pages.findIndex(p => start >= p.start && start < p.end && p.index == current )//定位章节
									if ( pageIndex == -1 ) this.current = this.pages.findIndex(p => start >= p.end && p.index == current )//定位章节
									else this.current = pageIndex
									this.$nextTick(function () {
										this.$refs.scroll.resetRefresh()
										this.$refs.scroll.scrollToIndex(this.current)
										this.handleChange({current: this.current, detail: this.pages[this.current] })
										this.$nextTick(function () {
											this.autoplaySync = this.autoplay
										})
									})
								}
							}
						})
					}
					handle()
				}, 100)
			})
		},
		//翻页改变事件
		async handleScroll (e) {
			const scrollTop = e.detail.scrollTop
			const scrollHeight = e.detail.scrollHeight
			if ( !this.scrolling ) {
				try{
					this.scrolling = true
					const rate = Math.floor(scrollTop / this.contentHeight)
					let maybe = this.pages[rate] ? rate : this.pages.length-1
					let top = -1
					while ( top < 0 && maybe < this.pages.length - 1 ) {
						const itemRect = await this.$refs.scroll.getItemRect(maybe)
						top = itemRect.top
						top < 0 ? maybe++ : null
					}
					const current = top >= 0 ? maybe : this.pages.length - 1
					if ( current != this.current ) {
						this.handleChange({current: current, detail: this.pages[current]})
						this.$refs.footer && this.$refs.footer.refresh()
					}
					this.scrolling = false
				}catch(e){
					this.scrolling = false
				}
			}
		},
		//渲染下一章节
		handleScrollLoadRender (status, chapter, callback, isPrev) {
			if ( status == 'success' ) {//获取内容成功
				const nowIndex = isPrev ? this.pages[0].index : this.pages[this.pages.length-1].index
				const chapterIndex = this.chapters.findIndex(c => c.index == chapter.index)//是否已经包含相同章节
				if (chapterIndex > -1) this.chapters[chapterIndex] = chapter//如果包含则更新
				else this.chapters.push(chapter)//否则添加新章节
				const computedId = this.getComputedId()
				this.computeds.push(computedId)
				const computedIndex = this.computeds.indexOf(computedId)
				this.$nextTick(function () {
					setTimeout(() => {
						this.$refs.computed[computedIndex] && this.$refs.computed[computedIndex].start({
							chapter: chapter,
							success: p => {
								this.computeds.splice(computedIndex, 1)
								callback && callback( (chapter.isStart || chapter.isEnd) ? 'end' : status)//关闭加载动画
								const pages = isPrev ? p.concat(this.pages) : this.pages.concat(p)//添加新计算的章节内容
								this.pages = pages//渲染章节内容
								if ( isPrev ) {//如果是加载上一章节需要重新定位
									this.$nextTick(function () {
										this.current = pages.findIndex(page => page.index == nowIndex)//定位页面
										this.$refs.scroll.scrollToIndex(this.current)//刷新翻页组件
									})
								}
								this.chapterLoading = false//关闭章节加载等待
							}
						})
					}, 100)
				})
			} else {
				this.chapterLoading = false//关闭章节加载等待
				callback && callback(status)//关闭加载动画
			}
		}
	}
}