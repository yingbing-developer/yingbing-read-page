const threshold = 50
export default {
	props: {
		//点击事件位置设置
		clickOption: {
			type: Object,
			default () {
				return {
					width: uni.upx2px(200),
					height: uni.upx2px(200),
					left: 'auto',
					top: 'auto'
				}
			}
		}
	},
	computed: {
		//点击区域左边位置
		clickLeft () {
			return typeof this.clickOption.left == 'number' ? this.clickOption.left : (this.windowWidth / 2) - (this.clickOption.width / 2)
		},
		//点击区域右边位置
		clickRight () {
			return this.clickLeft + this.clickOption.width
		},
		//点击区域顶部位置
		clickTop () {
			return typeof this.clickOption.top == 'number' ? this.clickOption.top : (this.windowHeight / 2) - (this.clickOption.height / 2)
		},
		//点击区域底部位置
		clickBottom () {
			return this.clickTop + this.clickOption.height
		}
	},
	data () {
		return {
			autoplaySync: false,
			windowHeight: 0,//组件高度
			windowWidth: 0,//组件宽度
			clickTime: 0,//点击次数
			touchstartX: 0,//触摸开始x轴位置
			touchstartY: 0,//触摸开始y轴位置
			touchmoveX: 0,//触摸滑动x轴距离
			touchmoveY: 0,//触摸滑动y轴距离
			touchTime: 0//触摸时间
		}
	},
	methods: {
		touchstart (e) {
			if ( this.pageType != 'scroll' ) this.autoplaySync = false//如果是翻页阅读关闭自动播放
			if ( this.touchTimer ) {//清楚定时任务
				clearTimeout(this.touchTimer)
				this.touchTimer = null
			}
			if ( this.longTimer ) {//清楚定时任务
				clearTimeout(this.longTimer)
				this.longTimer = null
			}
			const touch = e.touches[0]
			this.touchstartX = touch.pageX
			this.touchstartY = touch.pageY
			//点击在规定区域中
			if ( this.touchstartX >= this.clickLeft && this.touchstartX <= this.clickRight && this.touchstartY >= this.clickTop && this.touchstartY <= this.clickBottom) {
				this.clickTime++
				if ( this.clickTime == 1 ) {//第一次点击 
					this.touchTimer = setTimeout(() => {
						this.touchTime = 300
					}, 300)
					this.longTimer = setTimeout(() => {
						this.touchTime = 500
						if ( this.touchmoveX <= threshold && this.touchmoveY <= threshold ) this.$emit('long-click')//抛出长按事件
						this.resetTouch()
					}, 500)
				}
			} else {//没有点击在规定区域中直接重置触摸事件
				this.resetTouch()
			}
		},
		touchmove (e) {
			if ( this.clickTime > 0 ) {
				const touch = e.touches[0]
				this.touchmoveX = Math.abs(touch.pageX - this.touchstartX)
				this.touchmoveY = Math.abs(touch.pageY - this.touchstartY)
			}
		},
		touchend (e) {
			if ( this.pageType != 'scroll' ) this.autoplaySync = this.autoplay//如果是横向翻页开启自动播放
			if ( this.touchTimer ) {//清楚按下定时任务
				clearTimeout(this.touchTimer);
				this.touchTimer = null
			}
			if ( this.longTimer ) {//清楚长按定时任务
				clearTimeout(this.longTimer)
				this.longTimer = null
			}
			//点击次数为零或者触摸时间超过300，或者滑动距离超过阙值
			if ( this.clickTime == 0 || this.touchTime > 300 || this.touchmoveX > threshold || this.touchmoveY > threshold ) {
				this.resetTouch()
				return
			}
			if ( this.clickTime == 1 ) {//第一次点击
				this.touchTimer = setTimeout(() => {
					this.$emit('single-click')//抛出单击事件
					this.resetTouch()
				}, 300)
			}
			if ( this.clickTime == 2 ) {//第二次点击
				this.$emit('double-click')//抛出双击事件
				this.resetTouch()
			}
		},
		resetTouch () {
			this.touchstartX = 0
			this.touchstartY = 0
			this.touchmoveX = 0
			this.touchmoveY = 0
			this.touchTime = 0
			this.clickTime = 0
		}
	}
}