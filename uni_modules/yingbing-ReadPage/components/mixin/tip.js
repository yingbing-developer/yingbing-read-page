export default {
	provide () {
		return {
			getPrevChapterDefaultText: () => this.prevChapterDefaultText,
			getNextChapterDefaultText: () => this.nextChapterDefaultText,
			getChapterReadyText: () => this.chapterReadyText,
			getChapterLoadingText: () => this.chapterLoadingText,
			getChapterSuccessText: () => this.chapterSuccessText,
			getChapterFailText: () => this.chapterFailText,
			getPrevChapterEndText: () => this.prevChapterEndText,
			getNextChapterEndText: () => this.nextChapterEndText
		}
	},
	props: {
		loadingText: {
			type: String,
			default: '内容排版中'
		},
		errorText: {
			type: String,
			default: '渲染失败\n\n点击重试'
		},
		chapterReadyText: {
			type: String,
			default: '松开加载'
		},
		chapterLoadingText: {
			type: String,
			default: '正在加载'
		},
		chapterSuccessText: {
			type: String,
			default: '加载成功'
		},
		chapterFailText: {
			type: String,
			default: '加载失败'
		},
		prevChapterDefaultText: {
			type: String,
			default: '加载上一章'
		},
		prevChapterEndText: {
			type: String,
			default: '前面没有了'
		},
		nextChapterDefaultText: {
			type: String,
			default: '加载下一章'
		},
		nextChapterEndText: {
			type: String,
			default: '后面没有了'
		}
	}
}