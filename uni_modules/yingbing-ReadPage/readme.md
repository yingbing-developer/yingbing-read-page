#使用须知

* 1、这是一个小说分页插件，不包含设置窗口
* 2、这个插件有3个组件，分别是yingbing-html-reader | yingbing-text-reader | yingbing-whole-reader
* 3、插件本身只支持滚动阅读，如果需要翻页功要配合[好用翻页插件](https://ext.dcloud.net.cn/plugin?id=13712)同时下载使用
* 4、h5页面电量显示只能在安全环境下获取（比如：https://、file:///、localhost://）
* 5、使用本插件一定要仔细阅读下面的文档，以及下载示例
* 6、有什么不懂，可以加QUN 1087735942 聊


#yingbing-html-reader html文本阅读插件 只支持（APP-VUE || H5 || MP-WEIXIN）
* 微信小程序使用这个组件性能较差，尽量避免一段很长的文本，最好将每句话都单独用p标签包裹
* 传入的chapter章节序号必须是大于0的正整数
* 组件提供了2个特殊的标签full-box和whole-render，whole-render标签包裹的所有内容都是整体渲染，不会被切割，full-box继承了内容区域的全部高度，其余和whole-render一样
* full-box标签和whole-render标签不要包裹使用
* 一定要保证标签的完整性比如&lt;p&gt;标签后面一定要接一个&lt;/p&gt;，否则可能会导致排版错乱，微信小程序极大的增加排版时间
* props属性

| 属性名 | 类型 | 默认值 | 可选值 | 说明 |
| :----- | :----: | :----: | :----: | :---- |
| autoplay | Boolean | false | true/false | 自动翻页 |
| interval | String/Number | 5000 | ---- | 自动翻页周期 |
| pageType | String | scroll | real/cover/scroll/none | 翻页模式 |
| split | String | '' | ---- | 分隔符排版字符串（默认为空表示单个字符分隔计算排版，如果传入如空格符，则表示以空格分隔字符串进行计算排版，适用于英文小说） |
| color | String | #333333 | ---- | 字体颜色 |
| fontSize | String/Number | 15 | ---- | 字体大小 |
| fontFamily | String | Arial | ---- | 字体名称 |
| background | String | #fcd281 | ---- | 背景颜色（支持css渐变） |
| lineHeight | String/Number | fontSize + 15 | ---- | 行高 |
| slide | String/Number | 40 | ---- | 页面左右边距 |
| topGap | String/Number | 10 | ---- | 页面上边距 |
| bottomGap | String/Number | 10 | ---- | 页面下边距 |
| headerShow | Boolean | true | true/false | 显示顶部内容 |
| footerShow | Boolean | true | true/false | 显示底部内容 |
| backShow | Boolean | false | true/false | 显示顶部返回按钮 |
| preloadable | Boolean | false | true/false | 开启预加载章节功能 |
| selectable | Boolean | false | true/false | 开启文本选择 |
| loadPageNum | Number/String | 5 | ---- | 距离最后还有多少页时开始加载下一章节（需要配合preloadable属性使用） |
| unableClickPage | Boolean | false | true/false | 是否关闭点击左右2边位置翻页（pageType为none时忽略此属性） |
| totalChapter | Number/String | 0 | ---- | 总章节数量用于计算顶部右上角的阅读进度 |
| clickOption | Object | { width: uni.upx2px(200),height: uni.upx2px(200),left:'auto',top:'auto'} | ---- | 点击区域配置（点击哪个区域有效 enableClick为true时有效） |
| loadingText | String | '内容排版中' | ---- | 初始化或者跳转等待的提示文字 |
| errorText | String | '渲染失败\n\n点击重试' | ---- | 初始化或者跳转失败的提示文字 |
| prevChapterDefaultText | String | '加载上一章' | ---- | 加载上一章的默认文字 |
| nextChapterDefaultText | String | '加载下一章' | ---- | 加载下一章的默认文字 |
| chapterReadyText | String | '松开加载' | ---- | 章节准备加载的提示文字 |
| chapterLoadingText | String | '正在加载' | ---- | 章节加载中的提示文字 |
| chapterSuccessText | String | '加载成功' | ---- | 章节加载成功的提示文字 |
| chapterFailText | String | '加载失败' | ---- | 章节加载失败的提示文字 |
| prevChapterEndText | String | '前面没有了' | ---- | 前面章节加载完毕的提示文字 |
| nextChapterEndText | String | '后面没有了' | ---- | 后面章节加载完毕的提示文字 |

* clickOption介绍

| 键名 | 类型 | 说明 |
| :----- | :----: | :---- |
| width | Number | 点击区域宽度 |
| height | Number | 点击区域高度 |
| left | String/Number | 左右定位（默认auto为居中，可传入数字） |
| top | String/Number | 上下定位（默认auto为居中，可传入数字） |

* event事件

| 事件名 | 说明 |
| :----- | :---- |
| loadmore | 加载章节内容事件 |
| change | 阅读页面改变触发事件（返回当前阅读页面信息）|
| back | 点击顶部返回按钮事件 |
| single-click | 单击事件（配合clickOption使用） |
| double-click | 双击事件（配合clickOption使用） |
| long-click | 长按事件（配合clickOption使用） |

* 内置方法

| 方法名 | 说明 |
| :--- | :---- |
| init | 初始化小说内容 |
| change | 跳转小说位置 |
| refresh | 刷新阅读页面 |
| next | 翻到下一页 （滚动模式下表现为向下滚动一段距离） |
| prev | 翻到上一页 （滚动模式下表现为向上滚动一段距离） |
| setContent | 更新当前页面的内容（主要用于更改文字颜色等不会影响排版的样式） |

* 支持的文本格式
```javascript
const text1 = '文本文本文本\r\n文本文本'//组件会自动替换换行符为<br/>，如果要使用纯文本格式建议使用yingbing-text-reader组件
const text2 = '<p>文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本</p><p>文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本</p>'
const text3 = '文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本<br/>文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本'
```

* 插件使用
```html
	<!-- 只是一个参考，具体样式需要自行设计，必须保证组件能获得高度 -->
	<yingbing-html-reader ref="reader" style="height: 100vh;"
	back-show
	preloadable
	@single-click="handleSingleClick"
	@double-click="handleDoubleClick"
	@long-click="handleLongClick"
	@change="handleChange"
	@back="handleBack"
	@loadmore="handleLoadmore">
	
		<!-- 插槽使用方式 -->
		<!-- #ifdef APP-VUE || H5 -->
		<template v-slot:vip="{item, index}">
		<!-- #endif -->
		<!-- #ifdef MP-WEIXIN -->
		<template v-for="(item, index) in chapters" :slot="`vip:${item.index}`">
		<!-- #endif -->
			<view class="slots">第{{item.index}}章节付费阅读提示页</view>
		</template>
		<!-- #ifdef APP-VUE || H5 -->
		<template v-slot:end="{item, index}">
		<!-- #endif -->
		<!-- #ifdef MP-WEIXIN -->
		<template v-for="(item, index) in chapters" :slot="`end:${item.index}`">
		<!-- #endif -->
			<view class="slots">第{{item.index}}章节阅读完成提示页</view>
		</template>
	</yingbing-html-reader>
	<!-- 演示跳转内容 -->
	<button style="position:fixed;bottom: 20px;left:0;right:0;" @tap="handleJump">跳转</button>
```
```javascript
	export default {
		data () {
			return {
				chapters: []
			}
		},
		onReady () {
			for ( let i = 0 ; i < 10; i++ ) this.chapters.push(this.getChapter(i + 1))
			const chapters = [this.getChapter(1), this.getChapter(2)]//获取第一、第二章节 
			//title是小说标题
			//current用于定位章节
			//start用于定位章节内容的位置索引
			this.$refs.reader.init({chapters, title: '黑暗世界', current: 1, start: 0})
		},
		methods: {
			getChapter (index) {
				return {
					title: '第' + index + '章',//章节标题
					content: this.getContent(index),//章节内容
					frontSlots: ['vip'],//前置插槽集合
					backSlots: ['end'],//后置插槽集合
					index: index,//章节序号 必须是大于0的正整数
					isStart: index == 1,//是否第一章节
					isEnd: index == 10//是否最后章节
				}
			},
			//获取内容，想要减少微信小程序的排版时间，尽量将每一句话都用p标签包裹
			getContent (chapter) {
				return `
				<full-box style="display:flex;flex-direction:column;justify-content:center;">
					<h2 style="text-align:center;">诡异世界</h2>
					<div style="text-align:center;font-size: 20px">第${chapter}章</div>
				</full-box>
				<img src="https://img-cdn-tx.dcloud.net.cn/stream/plugin_screens/3e66f6a0-f3bb-11ee-aa3f-f11fc73dd829_0.jpg?&v=1712398249" style="height: 300px">
				<div style="color:blue">
					<p style="text-indent: 2em"><font style="color:red;">红中发黑的文字</font>是必须执行的剧情，否则只有死路一条。</p>
					<p style="text-indent: 2em">作为可以交易鬼物的死魂纸，可以根据该人探知真相进度的贡献度来获得，而篡改关键性<font style="color:red;">剧情，会没收一定数量的死魂纸。</font></p>
					<p style="text-indent: 2em">但这并不代表扮演者要完全忠诚地按照剧本说的去做，若果真如此，也只有死这一条路可走！相反，<font style="color:red;">往往需要扮演者打破</font>剧本中固有的剧情，才能够赢得一丝生机！</p>
					<p style="text-indent: 2em">页面上浮现了一行血字：</p>
					<p style="text-indent: 2em">【是否打开鬼物商城？】</p>
					<p style="text-indent: 2em">是。</p>
				</div>
				<whole-render>
					<div style="background-color:gold;height:20px"></div>
					<div style="background-color:green;height:20px"></div>
					<div style="background-color:blue;height:20px"></div>
				</whole-render>
				<p style="text-indent: 2em">陈献的眼前出现了一个黑色的页面，边框熊熊燃烧着地狱之火，炙人眼球。</p>
				<p style="text-indent: 2em">大概二十多个鬼物被挂在页面上，几乎都是动辄需要一两千张死魂纸的鬼物。</p>
				<p style="text-indent: 2em">目前陈献的100张死魂纸，只够购买一个叫【死亡人眼】的鬼物。</p>
				<p style="text-indent: 2em">【死亡人眼】：共可使用三次，已被使用两次，剩余可击退一次轻级诅咒鬼魂的攻击，对中级诅咒鬼魂有轻微抑制性作用，对高级和顶级诅咒鬼魂无用。卖家：盲人女孩扮演者凌小薇。（在《猫脸老太太》恐怖剧本中可获得）</p>
				<p style="text-indent: 2em">页面的血字发生了改变。</p>
				<p style="text-indent: 2em">【是否使用二十张死魂纸刷新页面？】</p>
				<p style="text-indent: 2em">现在这个页面他能勉强买的起的就只有一个【死亡人眼】，再用20张死魂纸刷新，恐怕一个也买不了。</p>
				<p style="text-indent: 2em">陈献当机立断，购买了【死亡人眼】</p>
				<p style="text-indent: 2em">【鬼物：死亡人眼死魂纸数量：0】</p>
				<p style="text-indent: 2em">交易的瞬间，他的手心出现了一种圆润滑腻的触感，带着股若有若无的腥臊味。</p>
				<p style="text-indent: 2em">陈献打开手心一看，里面有一只玻璃球大小的眼球，上眼皮黏连在上面，很像荔枝，而眼球的瞳仁正对着他。</p>
				<p style="text-indent: 2em">一种悚然的感觉入骨，他放入了裤兜里。</p>
				<p style="text-indent: 2em">陈献目前考虑的是，既然鬼物是抵抗鬼魂的唯一手段，那么他就需要更多的死魂纸来购买鬼物。</p>
				<p style="text-indent: 2em">还有就是，待会儿发现孙晗死亡的是他，他要不要稍微改变一下剧情？比如说，叫上男主角梁鸿一起去发现死亡的孙晗？</p>
				<p style="text-indent: 2em">不过，现在无论他怎么想都是无用的，要看到时的情况，总之，他想要主动地去探索这个恐怖剧本背后的真相，来获得死魂纸，从而囤积作为保命手段的鬼物。</p>
				<p style="text-indent: 2em">时间在一点一点的过去，轻微的鼾声逐渐响起，陈献却全然不同，一想到接下来要发生的事情，他就无法睡眠。</p>
				<p style="text-indent: 2em">好冷……比之前更冷了……</p>
				<p style="text-indent: 2em">每呼吸一次，都好像吸了一口碎冰渣似的，喉咙都被扎得生疼。</p>
				<p style="text-indent: 2em">陈献缩了缩身体，将自己裹得更紧些，虽然这并不能让他暖和哪怕一点。</p>
				<p style="text-indent: 2em">困意汹涌地袭来，眼皮像被重物压上一样越来越重，陈献慢慢失去意识，睡了过去……</p>
				<p style="text-indent: 2em">“吱——嘎——！”</p>
				<p style="text-indent: 2em">陈献睁开双眼，巨大的声响仿佛平地一声惊雷，将他猛然惊醒！</p>
				<p style="text-indent: 2em">旧蓝色的宿舍门竟然开了一道缝隙！</p>
				<p style="text-indent: 2em">黑黢黢的缝隙后好像隐藏了一只细长的眼睛，让陈献越看越心慌，如果他没记错，门本来是锁着的。</p>
				<p style="text-indent: 2em">外面不知何时下起了雨，打着窗棂发出“啪啪”的脆响。</p>
				<p style="text-indent: 2em">“扣………扣…………扣……”</p>
				<p style="text-indent: 2em">门被从外面敲击着，敲击声速度很慢，好像一个八十老妪在用苍老的手指一下一下敲着。</p>
				<p style="text-indent: 2em">“扣……扣…扣”</p>
				<p style="text-indent: 2em">敲击的速度明显加快了。</p>
				<p style="text-indent: 2em">陈献犹豫着，想来想去，还是在上铺装睡吧，他闭上了眼睛。</p>
				<p style="text-indent: 2em">敲门声停止了。</p>
				<p style="text-indent: 2em">静默又回归了寝室。</p>
				<p style="text-indent: 2em">突然，仿佛要把门撞碎一样，疯狂的敲门声刺入耳膜！</p>
				<p style="text-indent: 2em">“扣扣扣扣扣扣！！！！！！”</p>
				<p style="text-indent: 2em">下铺传来了缓慢的窸窸窣窣的脚步声。</p>
				<p style="text-indent: 2em">是谁出去了？可是先听到开门声再听到脚步声，那是有人进来了？是梁鸿吗？</p>
				<p style="text-indent: 2em">陈献摸到裤兜里的那个小凸起，手脚冰凉。</p>
				<p style="text-indent: 2em">敲门声停止了</p>
				<p style="text-indent: 2em">一声凄厉的婴儿哭声在寝室内响起，近在咫尺，带着一种在孕妇羊水里的稠密。</p>
				<p style="text-indent: 2em">“呜哇……呜哇……”</p>
				<p style="text-indent: 2em">没关系的……今晚死的人是孙晗……只要他一直保持这样的状态……</p>
				<p style="text-indent: 2em">又没有动静了，无论是敲门声，婴儿啼哭声还是脚步声，都消失了。</p>
				<p style="text-indent: 2em">剧烈的尿意盈满了陈献的膀胱。</p>
				<p style="text-indent: 2em">他竭力控制着自己的括约肌，甚至在想，要不直接就尿在床上得了……</p>
				<p style="text-indent: 2em">死魂纸数量：-2（如果在剧本结束后，死魂纸数量小于-20张，则判定死亡）</p>
				<p style="text-indent: 2em">看来必须要走剧情了，陈献赶紧睁开眼睛坐起了身子，四周一片漆黑，伸手不见五指。</p>
				<p style="text-indent: 2em">过段时间后，他的眼睛适应了黑暗，稍微能看清一些东西了。</p>
				<p style="text-indent: 2em">他看向孙晗那边，那儿的被子里有个背对着他的人形，看不出死活。</p>
				<p style="text-indent: 2em">既然要发现他的死亡，只能下去了，再找个借口叫一下孙晗。</p>
				<p style="text-indent: 2em">陈献小心翼翼地踩着梯子下去，结果突然看到梁鸿坐在凳子上背对着他，捧着个电脑，里面放着影像。</p>
				<p style="text-indent: 2em">这是剧本没有的内容。</p>
				<p style="text-indent: 2em">“……梁鸿？你不睡觉在这里干什么？”</p>
				<p style="text-indent: 2em">梁鸿没有理他，只见他的电脑里放着一个宿舍的影像，里面居然出现了陈献！</p>
				<p style="text-indent: 2em">影像高糊，还是黑白的，但陈献还是认出了自己。</p>
				<p style="text-indent: 2em">在影像里，陈献被尿意憋醒下了梯子，然后看到了抱着电脑的梁鸿，接着梁鸿转过头来，由于拍摄角度原因，陈献看不到影像里的梁鸿转头后的面目，但是影像里的陈献竟然无比惊骇地尖叫一声，七窍流血地倒在了地上，肠子内脏流了一地！</p>
				<p style="text-indent: 2em">冷汗顺着陈献的脸</p>`.replace(/\n/g, '')
			},
			handleBack () {
				console.log('点击了返回按钮')
			},
			//页面改变事件
			handleChange (e) {
				console.log('change', e);
				if ( e.detail.current == 2 ) {//更新页面内容
					this.$refs.reader.setContent(e.current, e.detail.content.replace('text-align:center;', 'text-align:center;color:red;'))
				}
			},
			handleSingleClick () {
				console.log('单击');
			},
			handleDoubleClick () {
				console.log('双击');
			},
			handleLongClick () {
				console.log('长按');
			},
			//加载章节内容
			handleLoadmore (current, callback) {
				setTimeout(() => {//模拟请求
					callback('success', this.getChapter(current))
				}, 300)
			},
			handleJump () {
				//chapters为章节列表集合
				//current为章节序号索引
				//start为章节内容位置索引
				//这是个演示，实际使用时，只需要调用一次就行
				this.$refs.reader.change({current: 3, start: 462})//不传入chapters章节列表，如果插件内部记录了该章节内容，会直接渲染，没有则会触发loadmore请求章节内容后再渲染
				this.$refs.reader.change({chapters: [this.getChapter(3)], current: 3, start: 462})//传入章节列表，避免触发loadmore，也可以通过该方式更新章节内容
			}
		}
	}
```

#yingbing-text-reader 纯文本阅读插件 只支持（APP-VUE || H5 || MP-WEIXIN）
* 该组件和yingbing-html-reader差不多，只是渲染内容不同
* 传入的chapter章节序号必须是大于0的正整数

* props属性

| 属性名 | 类型 | 默认值 | 可选值 | 说明 |
| :----- | :----: | :----: | :----: | :---- |
| autoplay | Boolean | false | true/false | 自动翻页 |
| interval | String/Number | 5000 | ---- | 自动翻页周期 |
| pageType | String | scroll | real/cover/scroll/none | 翻页模式 |
| split | String | '' | ---- | 分隔符排版字符串（默认为空表示单个字符分隔计算排版，如果传入如空格符，则表示以空格分隔字符串进行计算排版，适用于英文小说） |
| color | String | #333333 | ---- | 字体颜色 |
| fontSize | String/Number | 15 | ---- | 字体大小 |
| fontFamily | String | Arial | ---- | 字体名称 |
| measureSize | Object | ---- | ---- | 计算尺寸（具体见下面） |
| charSize | Array | ---- | ---- | 特别字符尺寸（具体见下面） |
| background | String | #fcd281 | ---- | 背景颜色（支持css渐变） |
| lineGap | String/Number | 15 | ---- | 行间距 |
| slide | String/Number | 40 | ---- | 页面左右边距 |
| topGap | String/Number | 10 | ---- | 页面上边距 |
| bottomGap | String/Number | 10 | ---- | 页面下边距 |
| headerShow | Boolean | true | true/false | 显示顶部内容 |
| footerShow | Boolean | true | true/false | 显示底部内容 |
| backShow | Boolean | false | true/false | 显示顶部返回按钮 |
| preloadable | Boolean | false | true/false | 开启预加载章节功能 |
| selectable | Boolean | false | true/false | 开启文本选择 |
| loadPageNum | Number/String | 5 | ---- | 距离最后还有多少页时开始加载下一章节（需要配合preloadable属性使用） |
| unableClickPage | Boolean | false | true/false | 是否关闭点击左右2边位置翻页（pageType为none时忽略此属性） |
| totalChapter | Number/String | 0 | ---- | 总章节数量用于计算顶部右上角的阅读进度 |
| clickOption | Object | { width: uni.upx2px(200),height: uni.upx2px(200),left:'auto',top:'auto'} | ---- | 点击区域配置（点击哪个区域有效 enableClick为true时有效） |

* clickOption介绍

| 键名 | 类型 | 说明 |
| :----- | :----: | :---- |
| width | Number | 点击区域宽度 |
| height | Number | 点击区域高度 |
| left | String/Number | 左右定位（默认auto为居中，可传入数字） |
| top | String/Number | 上下定位（默认auto为居中，可传入数字） |

* measureSize（用于分页计算，一般不用填，插件内部会自动测量，如果测量不对，可以自行填入修正）

| 键名 | 类型 | 默认值 | 说明 |
| :----- | :----: | :----: | :---- |
| lower | Number | 6 | 单个小写英文字母宽度 |
| upper | Number | 7 | 单个大写英文字母宽度 |
| number | Number | 5.5 | 单个数字宽度  |
| chinese | Number | 10 | 单个中文宽度  |
| tibetan | Number | 4.5 | 单个藏文宽度  |
| space | Number | 3.5 | 单个空格宽度 |
| special | Number | 8 | 单个特殊字符宽度 |
| other | Number | 10 | 其余字符宽度 |

* charSize（单独定义某些字符的尺寸，用于分页计算，例如：单独定义字符'a'和字符'b'的宽度可以定义为[{text: 'ab', size: 4}]）

| 键名 | 类型 | 说明 |
| :----- | :----: | :---- |
| text | String | 字符串 |
| size | Number | 字符尺寸（fontSize为10px） |

* event事件

| 事件名 | 说明 |
| :----- | :---- |
| loadmore | 加载章节内容事件 |
| change | 阅读页面改变触发事件（返回当前阅读页面信息）|
| back | 点击顶部返回按钮事件 |
| single-click | 单击事件（配合clickOption使用） |
| double-click | 双击事件（配合clickOption使用） |
| long-click | 长按事件（配合clickOption使用） |

* 内置方法

| 方法名 | 说明 |
| :--- | :---- |
| init | 初始化小说内容 |
| change | 跳转小说位置 |
| refresh | 刷新阅读页面 |
| next | 翻到下一页 （滚动模式下表现为向下滚动一段距离） |
| prev | 翻到上一页 （滚动模式下表现为向上滚动一段距离） |

* 支持的文本格式
```javascript
const text = '文本文本文本\r\n文本文本'
```

* 插件使用
```html
	<!-- 只是一个参考，具体样式需要自行设计，必须保证组件能获得高度 -->
	<yingbing-text-reader ref="reader" style="height: 100vh;"
	back-show
	preloadable
	@single-click="handleSingleClick"
	@double-click="handleDoubleClick"
	@long-click="handleLongClick"
	@change="handleChange"
	@back="handleBack"
	@loadmore="handleLoadmore">
	
		<!-- 插槽使用方式 -->
		<!-- #ifdef APP-VUE || H5 -->
		<template v-slot:vip="{item, index}">
		<!-- #endif -->
		<!-- #ifdef MP-WEIXIN -->
		<template v-for="(item, index) in chapters" :slot="`vip:${item.index}`">
		<!-- #endif -->
			<view class="slots">第{{item.index}}章节付费阅读提示页</view>
		</template>
		<!-- #ifdef APP-VUE || H5 -->
		<template v-slot:end="{item, index}">
		<!-- #endif -->
		<!-- #ifdef MP-WEIXIN -->
		<template v-for="(item, index) in chapters" :slot="`end:${item.index}`">
		<!-- #endif -->
			<view class="slots">第{{item.index}}章节阅读完成提示页</view>
		</template>
	</yingbing-text-reader>
	<!-- 演示跳转内容 -->
	<button style="position:fixed;bottom: 20px;left:0;right:0;" @tap="handleJump">跳转</button>
```
```javascript
	export default {
		data () {
			return {
				chapters: []
			}
		},
		onReady () {
			for ( let i = 0 ; i < 10; i++ ) this.chapters.push(this.getChapter(i + 1))
			const chapters = [this.getChapter(1), this.getChapter(2)]//获取第一、第二章节 
			//title是小说标题
			//current用于定位章节
			//start用于定位章节内容的位置索引
			this.$refs.reader.init({chapters, title: '黑暗世界', current: 1, start: 0})
		},
		methods: {
			getChapter (index) {
				return {
					title: '第' + index + '章',//章节标题
					content: this.getContent(index),//章节内容
					frontSlots: ['vip'],//前置插槽集合
					backSlots: ['end'],//后置插槽集合
					index: index,//章节序号 必须是大于0的正整数
					isStart: index == 1,//是否第一章节
					isEnd: index == 10//是否最后章节
				}
			},
			//获取内容
			getContent (chapter = 1) {
				return `第${chapter}章
				　　红中发黑的文字是必须执行的剧情，否则只有死路一条。\n　　作为可以交易鬼物的死魂纸，可以根据该人探知真相进度的贡献度来获得，而篡改关键性剧情，会没收一定数量的死魂纸。\n　　但这并不代表扮演者要完全忠诚地按照剧本说的去做，若果真如此，也只有死这一条路可走！相反，往往需要扮演者打破剧本中固有的剧情，才能够赢得一丝生机！\n　　页面上浮现了一行血字：\n　　【是否打开鬼物商城？】\n　　是。\n　　陈献的眼前出现了一个黑色的页面，边框熊熊燃烧着地狱之火，炙人眼球。\n　　大概二十多个鬼物被挂在页面上，几乎都是动辄需要一两千张死魂纸的鬼物。\n　　目前陈献的100张死魂纸，只够购买一个叫【死亡人眼】的鬼物。\n　　【死亡人眼】：共可使用三次，已被使用两次，剩余可击退一次轻级诅咒鬼魂的攻击，对中级诅咒鬼魂有轻微抑制性作用，对高级和顶级诅咒鬼魂无用。卖家：盲人女孩扮演者凌小薇。（在《猫脸老太太》恐怖剧本中可获得）\n　　页面的血字发生了改变。\n　　【是否使用二十张死魂纸刷新页面？】\n　　现在这个页面他能勉强买的起的就只有一个【死亡人眼】，再用20张死魂纸刷新，恐怕一个也买不了。\n　　陈献当机立断，购买了【死亡人眼】\n　　【鬼物：死亡人眼\n　　死魂纸数量：0】\n　　交易的瞬间，他的手心出现了一种圆润滑腻的触感，带着股若有若无的腥臊味。\n　　陈献打开手心一看，里面有一只玻璃球大小的眼球，上眼皮黏连在上面，很像荔枝，而眼球的瞳仁正对着他。\n　　一种悚然的感觉入骨，他放入了裤兜里。\n　　陈献目前考虑的是，既然鬼物是抵抗鬼魂的唯一手段，那么他就需要更多的死魂纸来购买鬼物。\n　　还有就是，待会儿发现孙晗死亡的是他，他要不要稍微改变一下剧情？比如说，叫上男主角梁鸿一起去发现死亡的孙晗？\n　　不过，现在无论他怎么想都是无用的，要看到时的情况，总之，他想要主动地去探索这个恐怖剧本背后的真相，来获得死魂纸，从而囤积作为保命手段的鬼物。\n　　时间在一点一点的过去，轻微的鼾声逐渐响起，陈献却全然不同，一想到接下来要发生的事情，他就无法睡眠。\n　　好冷……比之前更冷了……\n　　每呼吸一次，都好像吸了一口碎冰渣似的，喉咙都被扎得生疼。\n　　陈献缩了缩身体，将自己裹得更紧些，虽然这并不能让他暖和哪怕一点。\n　　困意汹涌地袭来，眼皮像被重物压上一样越来越重，陈献慢慢失去意识，睡了过去……\n　　“吱——嘎——！”\n　　陈献睁开双眼，巨大的声响仿佛平地一声惊雷，将他猛然惊醒！\n　　旧蓝色的宿舍门竟然开了一道缝隙！\n　　黑黢黢的缝隙后好像隐藏了一只细长的眼睛，让陈献越看越心慌，如果他没记错，门本来是锁着的。\n　　外面不知何时下起了雨，打着窗棂发出“啪啪”的脆响。\n　　“扣………扣…………扣……”\n　　门被从外面敲击着，敲击声速度很慢，好像一个八十老妪在用苍老的手指一下一下敲着。\n　　“扣……扣…扣”\n　　敲击的速度明显加快了。\n　　陈献犹豫着，想来想去，还是在上铺装睡吧，他闭上了眼睛。\n　　敲门声停止了。\n　　静默又回归了寝室。\n　　突然，仿佛要把门撞碎一样，疯狂的敲门声刺入耳膜！\n　　“扣扣扣扣扣扣！！！！！！”\n　　下铺传来了缓慢的窸窸窣窣的脚步声。\n　　是谁出去了？可是先听到开门声再听到脚步声，那是有人进来了？是梁鸿吗？\n　　陈献摸到裤兜里的那个小凸起，手脚冰凉。\n　　一声凄厉的婴儿哭声在寝室内响起，近在咫尺，带着一种在孕妇羊水里的稠密。\n　　“呜哇……呜哇……”\n　　没关系的……今晚死的人是孙晗……只要他一直保持这样的状态……\n　　又没有动静了，无论是敲门声，婴儿啼哭声还是脚步声，都消失了。\n　　剧烈的尿意盈满了陈献的膀胱。\n　　他竭力控制着自己的括约肌，甚至在想，要不直接就尿在床上得了……\n　　死魂纸数量：-2（如果在剧本结束后，死魂纸数量小于-20张，则判定死亡）\n　　看来必须要走剧情了，陈献赶紧睁开眼睛坐起了身子，四周一片漆黑，伸手不见五指。\n　　过段时间后，他的眼睛适应了黑暗，稍微能看清一些东西了。\n　　他看向孙晗那边，那儿的被子里有个背对着他的人形，看不出死活。\n　　既然要发现他的死亡，只能下去了，再找个借口叫一下孙晗。\n　　陈献小心翼翼地踩着梯子下去，结果突然看到梁鸿坐在凳子上背对着他，捧着个电脑，里面放着影像。\n　　这是剧本没有的内容。\n　　“……梁鸿？你不睡觉在这里干什么？”\n　　梁鸿没有理他，只见他的电脑里放着一个宿舍的影像，里面居然出现了陈献！\n　　影像高糊，还是黑白的，但陈献还是认出了自己。\n　　在影像里，陈献被尿意憋醒下了梯子，然后看到了抱着电脑的梁鸿，接着梁鸿转过头来，由于拍摄角度原因，陈献看不到影像里的梁鸿转头后的面目，但是影像里的陈献竟然无比惊骇地尖叫一声，七窍流血地倒在了地上，肠子内脏流了一地！\n　　冷汗顺着陈献的脸
				`
			},
			handleBack () {
				console.log('点击了返回按钮')
			},
			//页面改变事件
			handleChange (e) {
				console.log('change', e);
			},
			handleSingleClick () {
				console.log('单击');
			},
			handleDoubleClick () {
				console.log('双击');
			},
			handleLongClick () {
				console.log('长按');
			},
			//加载章节内容
			handleLoadmore (current, callback) {
				setTimeout(() => {//模拟请求
					callback('success', this.getChapter(current))
				}, 300)
			},
			handleJump () {
				//chapters为章节列表集合
				//current为章节序号索引
				//start为章节内容位置索引
				//这是个演示，实际使用时，只需要调用一次就行
				this.$refs.reader.change({current: 3, start: 462})//不传入chapters章节列表，如果插件内部记录了该章节内容，会直接渲染，没有则会触发loadmore请求章节内容后再渲染
				this.$refs.reader.change({chapters: [this.getChapter(3)], current: 3, start: 462})//传入章节列表，避免触发loadmore，也可以通过该方式更新章节内容
			}
		}
	}
```


#yingbing-whole-reader 全本小说（本地小说）阅读插件 只支持（APP-VUE || H5）
* props属性

| 属性名 | 类型 | 默认值 | 可选值 | 说明 |
| :----- | :----: | :----: | :----: | :---- |
| autoplay | Boolean | false | true/false | 自动翻页 |
| interval | String/Number | 5000 | ---- | 自动翻页周期 |
| pageType | String | scroll | real/cover/scroll/none | 翻页模式 |
| split | String | '' | ---- | 分隔符排版字符串（默认为空表示单个字符分隔计算排版，如果传入如空格符，则表示以空格分隔字符串进行计算排版，适用于英文小说） |
| color | String | #333333 | ---- | 字体颜色 |
| fontSize | String/Number | 15 | ---- | 字体大小 |
| fontFamily | String | Arial | ---- | 字体名称 |
| background | String | #fcd281 | ---- | 背景颜色（支持css渐变） |
| lineGap | String/Number | 15 | ---- | 行间距 |
| slide | String/Number | 40 | ---- | 页面左右边距 |
| topGap | String/Number | 10 | ---- | 页面上边距 |
| bottomGap | String/Number | 10 | ---- | 页面下边距 |
| headerShow | Boolean | true | true/false | 显示顶部内容 |
| footerShow | Boolean | true | true/false | 显示底部内容 |
| backShow | Boolean | false | true/false | 显示顶部返回按钮 |
| selectable | Boolean | false | true/false | 开启文本选择 |
| unableClickPage | Boolean | false | true/false | 是否关闭点击左右2边位置翻页（pageType为none时忽略此属性） |
| clickOption | Object | { width: uni.upx2px(200),height: uni.upx2px(200),left:'auto',top:'auto'} | ---- | 点击区域配置（点击哪个区域有效 enableClick为true时有效） |

* clickOption介绍

| 键名 | 类型 | 说明 |
| :----- | :----: | :---- |
| width | Number | 点击区域宽度 |
| height | Number | 点击区域高度 |
| left | String/Number | 左右定位（默认auto为居中，可传入数字） |
| top | String/Number | 上下定位（默认auto为居中，可传入数字） |

* event事件

| 事件名 | 说明 |
| :----- | :---- |
| change | 阅读页面改变触发事件（返回当前阅读页面信息）|
| back | 点击顶部返回按钮事件 |
| single-click | 单击事件（配合clickOption使用） |
| double-click | 双击事件（配合clickOption使用） |
| long-click | 长按事件（配合clickOption使用） |

* 内置方法

| 方法名 | 说明 |
| :--- | :---- |
| init | 初始化小说内容 |
| change | 跳转小说位置 |
| refresh | 刷新阅读页面 |
| next | 翻到下一页 （滚动模式下表现为向下滚动一段距离） |
| prev | 翻到上一页 （滚动模式下表现为向上滚动一段距离） |

* 支持的文本格式
```javascript
const text = '文本文本文本\r\n文本文本'
```

* 插件使用
```html
	<!-- 只是一个参考，具体样式需要自行设计，必须保证组件能获得高度 -->
	<yingbing-whole-reader ref="reader" style="height: 100vh;"
	back-show
	@single-click="handleSingleClick"
	@double-click="handleDoubleClick"
	@long-click="handleLongClick"
	@change="handleChange"
	@back="handleBack"></yingbing-text-reader>
	<!-- 演示跳转内容 -->
	<button style="position:fixed;bottom: 20px;left:0;right:0;" @tap="handleJump">跳转</button>
```
```javascript
	export default {
		onReady () {
			const content = this.getContent(1) + this.getContent(2) + this.getContent(3) + this.getContent(4)
			//title是小说标题
			//start用于定位内容的位置索引
			this.$refs.reader.init({content, start: 0, title: '黑暗世界'})
		},
		methods: {
			//获取内容
			getContent (chapter = 1) {
				return `第${chapter}章
				　　红中发黑的文字是必须执行的剧情，否则只有死路一条。\n　　作为可以交易鬼物的死魂纸，可以根据该人探知真相进度的贡献度来获得，而篡改关键性剧情，会没收一定数量的死魂纸。\n　　但这并不代表扮演者要完全忠诚地按照剧本说的去做，若果真如此，也只有死这一条路可走！相反，往往需要扮演者打破剧本中固有的剧情，才能够赢得一丝生机！\n　　页面上浮现了一行血字：\n　　【是否打开鬼物商城？】\n　　是。\n　　陈献的眼前出现了一个黑色的页面，边框熊熊燃烧着地狱之火，炙人眼球。\n　　大概二十多个鬼物被挂在页面上，几乎都是动辄需要一两千张死魂纸的鬼物。\n　　目前陈献的100张死魂纸，只够购买一个叫【死亡人眼】的鬼物。\n　　【死亡人眼】：共可使用三次，已被使用两次，剩余可击退一次轻级诅咒鬼魂的攻击，对中级诅咒鬼魂有轻微抑制性作用，对高级和顶级诅咒鬼魂无用。卖家：盲人女孩扮演者凌小薇。（在《猫脸老太太》恐怖剧本中可获得）\n　　页面的血字发生了改变。\n　　【是否使用二十张死魂纸刷新页面？】\n　　现在这个页面他能勉强买的起的就只有一个【死亡人眼】，再用20张死魂纸刷新，恐怕一个也买不了。\n　　陈献当机立断，购买了【死亡人眼】\n　　【鬼物：死亡人眼\n　　死魂纸数量：0】\n　　交易的瞬间，他的手心出现了一种圆润滑腻的触感，带着股若有若无的腥臊味。\n　　陈献打开手心一看，里面有一只玻璃球大小的眼球，上眼皮黏连在上面，很像荔枝，而眼球的瞳仁正对着他。\n　　一种悚然的感觉入骨，他放入了裤兜里。\n　　陈献目前考虑的是，既然鬼物是抵抗鬼魂的唯一手段，那么他就需要更多的死魂纸来购买鬼物。\n　　还有就是，待会儿发现孙晗死亡的是他，他要不要稍微改变一下剧情？比如说，叫上男主角梁鸿一起去发现死亡的孙晗？\n　　不过，现在无论他怎么想都是无用的，要看到时的情况，总之，他想要主动地去探索这个恐怖剧本背后的真相，来获得死魂纸，从而囤积作为保命手段的鬼物。\n　　时间在一点一点的过去，轻微的鼾声逐渐响起，陈献却全然不同，一想到接下来要发生的事情，他就无法睡眠。\n　　好冷……比之前更冷了……\n　　每呼吸一次，都好像吸了一口碎冰渣似的，喉咙都被扎得生疼。\n　　陈献缩了缩身体，将自己裹得更紧些，虽然这并不能让他暖和哪怕一点。\n　　困意汹涌地袭来，眼皮像被重物压上一样越来越重，陈献慢慢失去意识，睡了过去……\n　　“吱——嘎——！”\n　　陈献睁开双眼，巨大的声响仿佛平地一声惊雷，将他猛然惊醒！\n　　旧蓝色的宿舍门竟然开了一道缝隙！\n　　黑黢黢的缝隙后好像隐藏了一只细长的眼睛，让陈献越看越心慌，如果他没记错，门本来是锁着的。\n　　外面不知何时下起了雨，打着窗棂发出“啪啪”的脆响。\n　　“扣………扣…………扣……”\n　　门被从外面敲击着，敲击声速度很慢，好像一个八十老妪在用苍老的手指一下一下敲着。\n　　“扣……扣…扣”\n　　敲击的速度明显加快了。\n　　陈献犹豫着，想来想去，还是在上铺装睡吧，他闭上了眼睛。\n　　敲门声停止了。\n　　静默又回归了寝室。\n　　突然，仿佛要把门撞碎一样，疯狂的敲门声刺入耳膜！\n　　“扣扣扣扣扣扣！！！！！！”\n　　下铺传来了缓慢的窸窸窣窣的脚步声。\n　　是谁出去了？可是先听到开门声再听到脚步声，那是有人进来了？是梁鸿吗？\n　　陈献摸到裤兜里的那个小凸起，手脚冰凉。\n　　一声凄厉的婴儿哭声在寝室内响起，近在咫尺，带着一种在孕妇羊水里的稠密。\n　　“呜哇……呜哇……”\n　　没关系的……今晚死的人是孙晗……只要他一直保持这样的状态……\n　　又没有动静了，无论是敲门声，婴儿啼哭声还是脚步声，都消失了。\n　　剧烈的尿意盈满了陈献的膀胱。\n　　他竭力控制着自己的括约肌，甚至在想，要不直接就尿在床上得了……\n　　死魂纸数量：-2（如果在剧本结束后，死魂纸数量小于-20张，则判定死亡）\n　　看来必须要走剧情了，陈献赶紧睁开眼睛坐起了身子，四周一片漆黑，伸手不见五指。\n　　过段时间后，他的眼睛适应了黑暗，稍微能看清一些东西了。\n　　他看向孙晗那边，那儿的被子里有个背对着他的人形，看不出死活。\n　　既然要发现他的死亡，只能下去了，再找个借口叫一下孙晗。\n　　陈献小心翼翼地踩着梯子下去，结果突然看到梁鸿坐在凳子上背对着他，捧着个电脑，里面放着影像。\n　　这是剧本没有的内容。\n　　“……梁鸿？你不睡觉在这里干什么？”\n　　梁鸿没有理他，只见他的电脑里放着一个宿舍的影像，里面居然出现了陈献！\n　　影像高糊，还是黑白的，但陈献还是认出了自己。\n　　在影像里，陈献被尿意憋醒下了梯子，然后看到了抱着电脑的梁鸿，接着梁鸿转过头来，由于拍摄角度原因，陈献看不到影像里的梁鸿转头后的面目，但是影像里的陈献竟然无比惊骇地尖叫一声，七窍流血地倒在了地上，肠子内脏流了一地！\n　　冷汗顺着陈献的脸
				`
			},
			handleBack () {
				console.log('点击了返回按钮')
			},
			//页面改变事件
			handleChange (e) {
				console.log('change', e);
			},
			handleSingleClick () {
				console.log('单击');
			},
			handleDoubleClick () {
				console.log('双击');
			},
			handleLongClick () {
				console.log('长按');
			},
			handleJump () {
				//传入一个位置索引
				this.$refs.reader.change(800)
			}
		}
	}
```